package com.example.restapi2.repository.response;

import com.example.restapi2.repository.entity.Product;
import com.example.restapi2.repository.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {
    @Select("SELECT * FROM products_tb")
    List<Product> findAllProducts();
    // We'd use @Result When If our Class's fields not match with table's columns spelling
    // @Result(property = "", column = "")

    @Select("""
            SELECT * FROM invoice_details_tb idt
            INNER JOIN products_tb p ON p.product_id = idt.product_id
            WHERE idt.invoice_id = #{invoiceId}
            """)
    List<Product> getProductByInvoiceId (Integer invoiceId);

    @Select("SELECT * FROM products_tb WHERE product_id = #{productId}")
    Product getProductById(Integer productId);



    @Delete("DELETE FROM products_tb WHERE product_id = #{productId}")
    boolean deleteProductById (Integer productId);

    @Insert("INSERT INTO products_tb(product_name, product_price) " +
            "VALUES (#{request.product_name}, #{request.product_price}) " +
            "RETURNING product_id")
    Integer addNewProduct(@Param("request") ProductRequest productRequest);


    @Update("""
            UPDATE products_tb
            SET product_name = #{request.product_name},
            product_price = #{request.product_price}
            WHERE product_id = #{productId}
            RETURNING product_id
            """)
    Integer updateProductById(@Param("request") ProductRequest productRequest, Integer productId);
}
