package com.example.restapi2.repository.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerResponse<T> {

    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL) // hide if not call
    private T payload;
    private HttpStatus httpStatus;
    private Timestamp timestamp;
}
