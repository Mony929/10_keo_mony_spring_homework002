package com.example.restapi2.repository.response;

import com.example.restapi2.repository.entity.Customer;
import com.example.restapi2.repository.request.CustomerRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface CustomerRepository {

    @Select("SELECT * FROM customers_tb")
    // We'd use @Result When If our Class's fields not match with table's columns spelling
    // @Result(property = "", column = "")
    List<Customer> findAllCustomers();

    @Select("SELECT * FROM customers_tb WHERE customer_id = #{customerId}")
    @Result(property = "customer_id", column = "customer_id")
    @Result(property = "customer_name", column = "customer_name")
    @Result(property = "customer_address", column = "customer_address")
    @Result(property = "customer_phone", column = "customer_phone")
    Customer getCustomerById(Integer customerId);

    @Delete("DELETE FROM customers_tb WHERE customer_id = #{customerId}")
    boolean deleteCustomerById(Integer customerId);

    @Insert("INSERT INTO customers_tb(customer_name, customer_address, customer_phone) " +
            "VALUES( #{request.customer_name},  #{request.customer_address}, " +
            "#{request.customer_phone}) RETURNING customer_id")
    Integer saveCustomer (@Param("request") CustomerRequest customerRequest);

    @Update("""
            UPDATE customers_tb SET customer_name = #{request.customer_name},
            customer_address = #{request.customer_address},
            customer_phone = #{request.customer_phone}
            WHERE customer_id = #{customerId}
            RETURNING customer_id
            """)
    Integer updateCustomerById (@Param("request") CustomerRequest customerRequest, Integer customerId);


}
