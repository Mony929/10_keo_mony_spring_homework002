package com.example.restapi2.repository.response;


import com.example.restapi2.repository.entity.Invoice;
import com.example.restapi2.repository.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {
    @Select("""
            SELECT * FROM invoice_tb LIMIT #{size} OFFSET #{size} * (#{page}-1)
            """)
    @Results(
            id = "invoiceMapper",
            value = {
                    @Result(property = "invoice_id", column = "invoice_id"),
                    @Result(property = "invoice_date", column = "invoice_date"),
                    @Result(property = "customer", column = "customer_id",
                            one = @One(select = "com.example.restapi2.repository.response.CustomerRepository.getCustomerById")
                    ),
                    @Result(property = "productList", column = "invoice_id",
                            many = @Many(select = "com.example.restapi2.repository.response.ProductRepository.getProductByInvoiceId")
                    ),
            }
    )
    List<Invoice> findAllInvoice(Integer page, Integer size);

    @Select("SELECT * FROM invoice_tb WHERE invoice_id = #{invoiceId}")
    @ResultMap("invoiceMapper")
    Invoice getInvoiceById (Integer invoiceId);

    @Insert("""
            INSERT INTO invoice_tb (invoice_date, customer_id)
            VALUES (#{request.invoice_date}, #{request.customer_id})
            RETURNING invoice_id
            """)
    @ResultMap("invoiceMapper")
    Integer saveInvoice (@Param("request") InvoiceRequest invoiceRequest);


    @Select("""
            INSERT INTO invoice_details_tb (product_id, invoice_id)
            VALUES (#{productId}, #{invoiceId})
            """)
//    @Result(property = "", column = "")
//    @Result(property = "", column = "",
//            one = @One(select = "")
//    )
//    @Result(property = "productListId", column = "invoice_id",
//
//            many = @Many(select = "com.example.restapi2.repository.response.ProductRepository.getProductByInvoiceId")
//    )
    Integer saveProductByInvoiceId (Integer productId, Integer invoiceId);
}
