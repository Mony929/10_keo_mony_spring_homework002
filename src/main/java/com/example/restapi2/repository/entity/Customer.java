package com.example.restapi2.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Customer {
    private int customer_id;
    private String customer_name;
    private String customer_address;
    private String customer_phone;
}
