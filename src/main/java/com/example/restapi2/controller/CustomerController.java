package com.example.restapi2.controller;


import com.example.restapi2.repository.entity.Customer;
import com.example.restapi2.repository.request.CustomerRequest;
import com.example.restapi2.repository.response.CustomerResponse;
import com.example.restapi2.service.CustomerService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;

import java.sql.Timestamp;
import java.util.List;


@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
    private final CustomerService customerService;
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/all")
    //  @Operation(summary = "Get all customers")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer(){
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .message("Get All Customers Successfully !!! ")
                .payload(customerService.getAllCustomer())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get-customer-by-id{id}")
    //  @Operation(summary = "Get customer by id")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable("id") Integer customerId){
        CustomerResponse<Customer> response = null;
        if(customerService.getCustomerById(customerId) != null){
            response = CustomerResponse.<Customer>builder()
                    .message("Get Customer By Id Successfully !!! ")
                    .payload(customerService.getCustomerById(customerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = CustomerResponse.<Customer>builder()
                    .message("Sorry, Customer Id not exist !!! ")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @DeleteMapping("/delete-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<String>> deleteCustomerById(@PathVariable("id") Integer customerId){
        CustomerResponse<String> response = null;
        if(customerService.deleteCustomerById(customerId)){
            response = CustomerResponse.<String>builder()
                    .message("Delete Customer By Id Successfully !!! ")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = CustomerResponse.<String>builder()
                    .message("Sorry, Customer Id not exist !!!")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("/add-new-customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest){
        Integer storeCustomerId = customerService.addNewCustomer(customerRequest);
        CustomerResponse<Customer> response = null;
        if(storeCustomerId != null){
            response = CustomerResponse.<Customer>builder()
                    .message("Add New Customer Successfully !!!")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/update-customer-by-id{id}")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomer(@RequestBody CustomerRequest customerRequest,
                                                                     @PathVariable("id") Integer customerId){
        Integer storeCustomerId = customerService.updateCustomerById(customerRequest, customerId);
        CustomerResponse<Customer> response = null;
        if(customerService.getCustomerById(customerId) != null){
            response = CustomerResponse.<Customer>builder()
                    .message("Update Customer By Id Successfully !!! ")
                    .payload(customerService.getCustomerById(storeCustomerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = CustomerResponse.<Customer>builder()
                    .message("Sorry, Customer Id not exist !!! ")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

}
