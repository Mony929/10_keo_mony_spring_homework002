package com.example.restapi2.controller;

import com.example.restapi2.repository.entity.Product;
import com.example.restapi2.repository.request.ProductRequest;
import com.example.restapi2.repository.response.ProductResponse;
import com.example.restapi2.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.ibatis.annotations.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/all")
    //    @Operation(summary = "Get all products ")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProducts(){
        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .message("Get All Product successfully !!! ")
                .payload(productService.getAllProducts())
                .httpStatus(HttpStatus.OK)
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get-product-by-id/{id}")
    public ResponseEntity<ProductResponse<Product>> getProductById (@PathVariable("id") Integer productId){
        ProductResponse<Product> response = null;
        if(productService.getProductById(productId) != null){
            response = ProductResponse.<Product>builder()
                    .message("Get Product By Id successfully !!! ")
                    .payload(productService.getProductById(productId))
                    .httpStatus(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = ProductResponse.<Product>builder()
                    .message("Sorry, Product Id not exist !!! ")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @DeleteMapping("/delete-product-by-id/{id}")
    //    @Operation(summary = "Delete product by Id")
    public ResponseEntity<ProductResponse<String>> deleteProductById(@PathVariable("id") Integer productId){
        ProductResponse<String> response = null;
        if(productService.deleteProductById(productId)){
            response = ProductResponse.<String>builder()
                    .message("Delete Product By Id Successfully !!! ")
                    .httpStatus(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = ProductResponse.<String>builder()
                    .message("Sorry, Product Id not exist !!! ")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("/add-new-product")
    public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody ProductRequest productRequest){
        Integer storeProductId = productService.addNewProduct(productRequest);
        ProductResponse<Product> response = null;
        if( storeProductId != null){
            response = ProductResponse.<Product>builder()
                    .message("Insert New Product Successfully !!! ")
                    .httpStatus(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            return null;
        }
    }

    @PutMapping("/update-product-by-id{id}")
    public ResponseEntity<ProductResponse<Product>> updateProductById (@RequestBody ProductRequest productRequest,
                                                                       @PathVariable("id") Integer productId){
        Integer storeProductId = productService.updateProductById(productRequest, productId);
        ProductResponse<Product> response = null;
        if( productService.getProductById(productId) != null){
            response = ProductResponse.<Product>builder()
                    .message("Update Product By Id Successfully !!! ")
                    .payload(productService.getProductById(storeProductId))
                    .httpStatus(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = ProductResponse.<Product>builder()
                    .message("Sorry, Product Id not exist !!! ")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }



}
