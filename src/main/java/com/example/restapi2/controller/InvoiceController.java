package com.example.restapi2.controller;


import com.example.restapi2.repository.entity.Invoice;
import com.example.restapi2.repository.request.InvoiceRequest;
import com.example.restapi2.repository.response.InvoiceResponse;
import com.example.restapi2.service.InvoiceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice")
public class InvoiceController {

    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/get-all-invoice")
    public ResponseEntity<InvoiceResponse<List<Invoice>>> getAllInvoice(@RequestParam Integer page,
                                                                        @RequestParam Integer size){
        InvoiceResponse<List<Invoice>> response = InvoiceResponse.<List<Invoice>>builder()
                .message("Get All Invoice Successfully !!! ")
                .payload(invoiceService.getAllInvoice(page, size))
                .httpStatus(HttpStatus.OK)
                .build();
        return ResponseEntity.ok(response);
    }
    @GetMapping("/get-invoice-by-id/{id}")
    public ResponseEntity<InvoiceResponse<Invoice>> getInvoiceById(@PathVariable("id") Integer invoiceId){
        InvoiceResponse<Invoice> response = null;
        if(invoiceService.getInvoiceById(invoiceId) != null){
            response = InvoiceResponse.<Invoice>builder()
                    .message("Get Invoice By Id Successfully !!! ")
                    .payload(invoiceService.getInvoiceById(invoiceId))
                    .httpStatus(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = InvoiceResponse.<Invoice>builder()
                    .message("Sorry, Invoice Id not exist !!! ")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("/add-new-invoice")
    public ResponseEntity<InvoiceResponse<Invoice>> addNewInvoice(@RequestBody InvoiceRequest invoiceRequest){
        InvoiceResponse<Invoice> response = null;
        Integer storeInvoiceId = invoiceService.addNewInvoice(invoiceRequest);
        if(storeInvoiceId != null){
            response = InvoiceResponse.<Invoice>builder()
                    .message("Add New Invoice Successfully !!! ")
                    .payload(invoiceService.getInvoiceById(storeInvoiceId))
                    .httpStatus(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = InvoiceResponse.<Invoice>builder()
                    .message("Sorry, Invoice Id not exist !!! ")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }
}
