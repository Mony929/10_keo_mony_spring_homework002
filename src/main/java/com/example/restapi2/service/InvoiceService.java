package com.example.restapi2.service;

import com.example.restapi2.repository.entity.Invoice;
import com.example.restapi2.repository.request.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice(Integer page, Integer size);
    Invoice getInvoiceById (Integer invoiceId);

    Integer addNewInvoice (InvoiceRequest invoiceRequest);
}
