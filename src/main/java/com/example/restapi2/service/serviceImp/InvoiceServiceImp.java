package com.example.restapi2.service.serviceImp;

import com.example.restapi2.repository.entity.Invoice;
import com.example.restapi2.repository.request.InvoiceRequest;
import com.example.restapi2.repository.response.InvoiceRepository;
import com.example.restapi2.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImp implements InvoiceService {
    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice(Integer page, Integer size) {
        return invoiceRepository.findAllInvoice(page, size);
    }



    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.getInvoiceById(invoiceId);
    }

    @Override
    public Integer addNewInvoice(InvoiceRequest invoiceRequest) {
        Integer storeInvoiceId = invoiceRepository.saveInvoice(invoiceRequest);
        for (Integer productId : invoiceRequest.getProductListId()){
            invoiceRepository.saveProductByInvoiceId(storeInvoiceId, productId);
        }
        return storeInvoiceId;
    }
}
