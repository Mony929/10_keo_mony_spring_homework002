package com.example.restapi2.service.serviceImp;

import com.example.restapi2.repository.entity.Customer;
import com.example.restapi2.repository.request.CustomerRequest;
import com.example.restapi2.repository.response.CustomerRepository;
import com.example.restapi2.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomers();
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    @Override
    public boolean deleteCustomerById(Integer customerId) {
        return customerRepository.deleteCustomerById(customerId);
    }

    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
    //        Integer customerId = customerRepository.saveCustomer(customerRequest);
        return customerRepository.saveCustomer(customerRequest);
    }

    @Override
    public Integer updateCustomerById(CustomerRequest customerRequest, Integer customerId) {
    //        Integer customerUpdateId = customerRepository.updateCustomerById(customerRequest, customerId);
        return customerRepository.updateCustomerById(customerRequest, customerId);
    }


}
