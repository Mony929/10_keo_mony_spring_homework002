package com.example.restapi2.service.serviceImp;

import com.example.restapi2.repository.entity.Product;
import com.example.restapi2.repository.request.CustomerRequest;
import com.example.restapi2.repository.request.ProductRequest;
import com.example.restapi2.repository.response.ProductRepository;
import com.example.restapi2.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAllProducts();
    }

    @Override
    public Product getProductById(Integer productId) {
        return productRepository.getProductById(productId);
    }

    @Override
    public boolean deleteProductById(Integer productId) {
        return productRepository.deleteProductById(productId);
    }

    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        Integer productId = productRepository.addNewProduct(productRequest);
        return productId;
    }

    @Override
    public Integer updateProductById(ProductRequest productRequest, Integer productId) {
        Integer productUpdateId = productRepository.updateProductById(productRequest, productId);
        return productUpdateId;
    }

//    @Override
//    public Integer updateProductById(ProductRequest productRequest, Integer product_id) {
//        return productRepository.u;
//    }

//    public Integer updateCustomerById(ProductRequest productRequest, Integer productId) {
//        return productRepository.u
//    }

//    public Integer updateCustomerById(CustomerRequest customerRequest, Integer customerId) {
//        return customerRepository.updateCustomerById(customerRequest, customerId);
//    }
}
