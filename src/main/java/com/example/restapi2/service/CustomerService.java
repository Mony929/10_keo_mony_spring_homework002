package com.example.restapi2.service;

import com.example.restapi2.repository.entity.Customer;
import com.example.restapi2.repository.request.CustomerRequest;

import java.util.List;


public interface CustomerService {
    List<Customer> getAllCustomer();
    Customer getCustomerById (Integer customerId);
    boolean deleteCustomerById (Integer customerId);
    Integer addNewCustomer (CustomerRequest customerRequest);

    Integer updateCustomerById (CustomerRequest customerRequest, Integer customerId);

}
