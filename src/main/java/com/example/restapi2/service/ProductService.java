package com.example.restapi2.service;

import com.example.restapi2.repository.entity.Product;
import com.example.restapi2.repository.request.ProductRequest;
import org.springframework.stereotype.Service;

import java.util.List;


public interface ProductService {
    List<Product> getAllProducts();
    Product getProductById(Integer productId);
    boolean deleteProductById (Integer productId);
    Integer addNewProduct (ProductRequest productRequest);
    Integer updateProductById (ProductRequest productRequest, Integer productId);
}
