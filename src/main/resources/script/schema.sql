CREATE TABLE customers_tb (
    customer_id SERIAL4 PRIMARY KEY ,
    customer_name VARCHAR(255) NOT NULL ,
    customer_address VARCHAR(255) NOT NULL ,
    customer_phone VARCHAR(50) NOT NULL
);

INSERT INTO customers_tb (customer_name, customer_address, customer_phone)
            VALUES('test',  'pp',
            '6543');

UPDATE customers_tb
SET customer_name = '1', customer_address = 'pp', customer_phone = '012333'
WHERE customer_id = 2;

CREATE TABLE products_tb (
    product_id SERIAL4 PRIMARY KEY ,
    product_name VARCHAR(255) NOT NULL ,
    product_price float4 NOT NULL
);

DELETE FROM products_tb WHERE product_id = 2;

-- UPDATE customers_tb SET customer_name = #{request.customer_name}, " +
--             "customer_address = #{request.customer_address}," +
--             "request.customer_phone;

UPDATE customers_tb SET customer_name = 'hi', customer_address = 'kp',
                        customer_phone = '012' WHERE customer_id = 9;

INSERT INTO products_tb VALUES (11, 'NAME', 2.5);


CREATE TABLE invoice_tb (
    invoice_id SERIAL4 PRIMARY KEY ,
    invoice_date TIMESTAMP ,
    customer_id INT REFERENCES customers_tb(customer_id)
);

CREATE TABLE invoice_details_tb (
    invoice_dt_id SERIAL4 PRIMARY KEY ,
    invoice_id INT REFERENCES invoice_tb(invoice_id) ON UPDATE CASCADE ON DELETE CASCADE ,
    product_id INT REFERENCES products_tb(product_id) ON UPDATE CASCADE ON DELETE CASCADE
);
